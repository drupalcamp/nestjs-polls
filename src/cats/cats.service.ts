import { Injectable } from '@nestjs/common';
import { Cat } from './interfaces/cat.interface';

@Injectable()
export class CatsService {
  findOne(id: number) {
    // throw new Error('Method not implemented.');
    return id;
  }
  private readonly cats: Cat[] = [];

  create(cat: Cat) {
    this.cats.push(cat);
  }

  findAll(): Cat[] {
    return this.cats;
  }

  findAllService(): Cat[] | PromiseLike<Cat[]> {
    // throw new Error('Method not implemented.');
    this.cats.push({ name: 'Gung', age: 45, breed: 'Admin' });
    return this.cats;
  }
}

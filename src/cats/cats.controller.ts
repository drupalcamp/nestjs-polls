import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ForbiddenException,
  ParseIntPipe,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  HttpException,
  UseGuards,
  SetMetadata,
  UseInterceptors,
} from '@nestjs/common';

import { CreateCatDto, UpdateCatDto } from './dto/create-cat.dto';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';
import { MyParseIntPipe } from 'src/my-parse-int.pipe';
import { AllExceptionFilter } from 'src/all-exceptions.filter';
import { RolesGuard } from 'src/roles.guard';
import { Roles } from 'src/roles.decorator';
import { LoggingInterceptor } from 'src/logging.interceptor';

@Roles('admin')
@Controller('cats')
// @UseFilters(new HttpExceptionFilter())
// @UseGuards(RolesGuard)
@UseInterceptors(LoggingInterceptor)
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Post()
  // create(@Res() res: Response) {
  //   // return 'This action adds a new cat';
  //   res.status(HttpStatus.CREATED).send();
  // }
  async create(@Body(new ValidationPipe()) CreateCatDto: CreateCatDto) {
    // throw new ForbiddenException();
    this.catsService.create(CreateCatDto);
  }

  // @Get()
  // findAll(@Query() query: ListAllEntities) {
  //   return `This action returns all cats (limit: ${query.limit} items)`;
  // }
  // findAll(@Res({ passthrough: true}) res: Response) {
  //   res.status(HttpStatus.OK).json([]);
  // }
  @Get('role')
  @Roles('auth_user')
  // @SetMetadata('roles', ['admin', 'gung', 'anyone'])
  // @Roles('admin', 'auth_user')
  async findAll(): Promise<Cat[]> {
    console.log(Roles);
    // console.log(Get('role'));
    return this.catsService.findAllService();
    // throw new HttpException({
    //   status: HttpStatus.FORBIDDEN,
    //   error: "This is custom message Gung",
    // }, HttpStatus.FORBIDDEN);
    throw new ForbiddenException();
    throw new AllExceptionFilter();
  }

  @Get()
  // @SetMetadata('roles', ['admin', 'gung', 'anyone'])
  async findAll2(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', new MyParseIntPipe()) id) {
    // return `This action returns a #${id} cat`;
    return this.catsService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {
    return `This action updates a #${id} cat`;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return `This action removes a #${id} cat`;
  }
}

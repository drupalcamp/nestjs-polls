import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // const roles = this.reflector.get<string[]>('roles', context.getHandler());
    // const roles = this.reflector.get<string[]>('roles', context.getClass());
    const roles = this.reflector.getAllAndMerge<string[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);


    if (!roles) {
      return true;
    }
    console.log(roles);
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    return validateRequest(request);
    // return matchRoles(roles, user.roles);
  }
}

function validateRequest(
  request: any,
): boolean | Promise<boolean> | Observable<boolean> {
  return true;
}

function matchRoles(roles: any, userRoles: any): boolean {
  return true;
}

import { ValidationPipe } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { NestFactory } from '@nestjs/core';
import { AllExceptionFilter } from './all-exceptions.filter';
import { AppModule } from './app.module';
import { logger } from './logger.middleware';
const port = 3001;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.use(logger)
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionFilter(httpAdapter));
  // app.useGlobalPipes(new ValidationPipe());

  await app.listen(port);
  console.log('The server is running on: http://localhost:' + port);
}
// Run the app.
bootstrap();
